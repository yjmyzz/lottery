package jimmy.lottery;

import jimmy.lottery.Algorithm.PREDICT_RESULT;

import org.junit.Test;

public class AlgorithmTest {

	@Test
	public void testPredict() throws Exception {
		int groupCount = 100;
		int rndNumberCount = 1680;
		int stepLen = 1580;
		Algorithm algorithm = new Algorithm(0, stepLen, rndNumberCount,
				groupCount, (byte) 12, (byte) 49);

		int testCount = rndNumberCount - stepLen;
		int rightCount = 0;
		String t = "";
		for (int i = 0; i < testCount; i++) {
			PREDICT_RESULT result = algorithm.predictFuture();
			if (result.equals(PREDICT_RESULT.INVALID_GROUP)) {
				// 无效分组，本次循环不算
				i--;
			} else {
				stepLen += 1;
				algorithm.setStepLen(stepLen);
				if (result.equals(PREDICT_RESULT.SUCCESS)) {
					// 命中率+1
					rightCount += 1;
					t += "T ";
				} else {
					t += "F ";
				}
			}
			System.out.println("----------------------完成进度:" + (i + 1) + "/"
					+ testCount + "----------------------");
		}

		System.out.println("命中率：" + (double) rightCount / testCount);
		System.out.println(t);

	}
}
